/* Aorta -- Amazingly ORganized TAsks
 * Copyright (C) 1997 Shawn T. Amundson
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "menus.h"

static GnomeUIInfo menu_main_task[] = {
    {GNOME_APP_UI_ITEM, "Create Task...", 
     NULL, NULL, NULL, NULL, 
     GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_NEW,
      'T', GDK_CONTROL_MASK, NULL},
    {GNOME_APP_UI_SEPARATOR},
    {GNOME_APP_UI_ITEM, "Exit", 
      NULL, NULL, NULL, NULL,
      GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_QUIT,
      'E', GDK_CONTROL_MASK, NULL},
    {GNOME_APP_UI_ENDOFINFO}
};

static GnomeUIInfo menu_main_edit[] = {
#define MENU_EDIT_CUT_POS 0
        {GNOME_APP_UI_ITEM, N_("Cut"), NULL, NULL, NULL,
                NULL, GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_CUT,
                'X', GDK_CONTROL_MASK, NULL},
#define MENU_EDIT_COPY_POS 1
        {GNOME_APP_UI_ITEM, N_("Copy"), NULL, NULL, NULL, 
                NULL, GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_COPY,
                'C', GDK_CONTROL_MASK, NULL},
#define MENU_EDIT_PASTE_POS 2
        {GNOME_APP_UI_ITEM, N_("Paste"), NULL, NULL, NULL, 
                NULL, GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_PASTE,
                'V', GDK_CONTROL_MASK, NULL},
        {GNOME_APP_UI_ENDOFINFO}
};

static GnomeUIInfo menu_main_help[] = {
    {GNOME_APP_UI_ITEM, N_("About Aorta Task Manager..."), NULL, NULL, NULL,
        NULL, GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_ABOUT,
        0, 0, NULL},
    {GNOME_APP_UI_ENDOFINFO}
};


static GnomeUIInfo menu_main[] = {
    {GNOME_APP_UI_SUBTREE, "Task", NULL, menu_main_task, NULL, NULL,
        GNOME_APP_PIXMAP_NONE, NULL, 0, 0, NULL},
    {GNOME_APP_UI_SUBTREE, "Edit", NULL, menu_main_edit, NULL, NULL,
        GNOME_APP_PIXMAP_NONE, NULL, 0, 0, NULL},
#define MENU_HELP_POS 2
    {GNOME_APP_UI_SUBTREE, "Help", NULL, menu_main_help, NULL, NULL,
        GNOME_APP_PIXMAP_NONE, NULL, 0, 0, NULL},
    {GNOME_APP_UI_ENDOFINFO}
};


/*
        {"<Main>/Task/Create Task...", NULL, NULL, NULL},
        {"<Main>/Task/Delete", NULL, NULL, NULL},
        {"<Main>/Task/<separator>", NULL, NULL, NULL},
        {"<Main>/Task/Print...", NULL, NULL, NULL},
        {"<Main>/Task/<separator>", NULL, NULL, NULL},
        {"<Main>/Task/Exit", NULL, NULL, NULL},
        {"<Main>/Edit/Cut", NULL, NULL, NULL},
        {"<Main>/Edit/Paste", NULL, NULL, NULL},
        {"<Main>/Edit/Copy", NULL, NULL, NULL},
        {"<Main>/Edit/Delete", NULL, NULL, NULL},
        {"<Main>/Filter/By Manager...", NULL, NULL, NULL},
        {"<Main>/Filter/By Status...", NULL, NULL, NULL},
        {"<Main>/Filter/By Priority...", NULL, NULL, NULL},
        {"<Main>/Filter/Personal Only", NULL, NULL, NULL},
        {"<Main>/Filter/<separator>", NULL, NULL, NULL},
        {"<Main>/Filter/No Filter", NULL, NULL, NULL},
        {"<Main>/Configure/Index Appearance...",NULL, NULL, NULL},
        {"<Main>/Window/Address Book", NULL, NULL, NULL},
        {"<Main>/Help/About...", NULL, NULL, NULL}
*/

void
set_main_menu(GnomeApp *app) {
  gnome_app_create_menus(app, menu_main);
  gtk_menu_item_right_justify(GTK_MENU_ITEM(menu_main[MENU_HELP_POS].widget));
}
