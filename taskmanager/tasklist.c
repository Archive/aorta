/* Aorta -- Amazingly ORganized TAsks
 * Copyright (C) 1997 Shawn T. Amundson
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <stdio.h>
#include "database.h"

void aorta_print_tasks (AortaTaskDB*, AortaTaskID id, int);

int 
main (int argc, char *argv[]) 
{
  AortaTaskDB *taskdb;
  
  taskdb = aorta_filedb_open_taskdb("taskdb.aortadb");
  aorta_print_tasks(taskdb, 0, 0);
  aorta_taskdb_close(taskdb);
}

void 
aorta_print_tasks (AortaTaskDB *taskdb, AortaTaskID parentid, int indent)
{
  AortaTaskID *children;
  gint nchildren;
  AortaTaskRecord *record;
  gint child;
  gint ind;

  aorta_taskdb_children (taskdb, parentid, &nchildren, &children);
  for (child = 0; child < nchildren; child++) {
    record = aorta_taskdb_get_record(taskdb, children[child]);
    for (ind=0; ind < indent; ind++)
      printf(" ");
    printf("%-20.20s %15.15s %d\n", record->name, record->status, record->priority);
    aorta_print_tasks(taskdb, record->id, indent+2);
    g_free(record);
  }
  g_free(children);
}
