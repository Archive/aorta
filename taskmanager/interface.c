/* Aorta -- Amazingly ORganized TAsks
 * Copyright (C) 1997 Shawn T. Amundson, James Mitchell
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "interface.h"
#include "menus.h"

#define NUM_TITLES 6


typedef struct _AortaToolbarMain AortaToolbarMain;
typedef struct _AortaStatusbar AortaStatusbar;
typedef struct _AortaCommentbox AortaCommentbox;

struct _AortaToolbarMain {
  GtkWidget *widget;
  GtkWidget *handlebox;
  GtkWidget *container;
};

struct _AortaStatusbar {
  GtkWidget *container;
  GtkWidget *hbox;
  GtkWidget *vbox;
  GtkWidget *vbox2;
  GtkWidget *separator;
  GtkWidget *frame;
  GtkWidget *label;
  GtkWidget *tlabel;
  GtkWidget *lframe;
  GtkWidget *tlframe;
};

struct _AortaCommentbox {
  GtkWidget *container;
  GtkWidget *table;
  GtkWidget *text;
  GtkWidget *label;
  GtkWidget *vscroll;
  GtkWidget *hscroll;
  GtkWidget *vbox;
  GtkWidget *hbox;
};


AortaToolbarMain* aorta_make_main_toolbar(void);
AortaStatusbar* aorta_make_statusbar(void);
AortaCommentbox* aorta_make_commentbox(void);

void
aorta_main_window (void)
{
  GtkWidget *app;
  GtkWidget *vbox;
  GtkWidget *clist;
  GtkWidget *clist_hbox;
  GtkWidget *paned;
  AortaToolbarMain *toolbar;
  AortaStatusbar *statusbar;
  AortaCommentbox *commentbox;
  gchar *titles[NUM_TITLES];
 
  titles[0] = "Task Name";
  titles[1] = "Manager";
  titles[2] = "Priority";
  titles[3] = "Status";
  titles[4] = "Start Date";
  titles[5] = "Deadline";

  /* Main Application Screen */
  app = gnome_app_new ("Aorta","Aorta");
  gtk_widget_set_usize(app, 550, 300);

  /* Menus */
  set_main_menu(GNOME_APP(app));

  vbox = gtk_vbox_new(1,1);
  gnome_app_set_contents(GNOME_APP(app), vbox);
  gtk_box_set_homogeneous(GTK_BOX(vbox), FALSE);

  toolbar = aorta_make_main_toolbar();
  gtk_box_pack_start(GTK_BOX(vbox), toolbar->container, FALSE, FALSE, 3);

  paned = gtk_vpaned_new();
  gtk_container_add(GTK_CONTAINER(vbox), paned);
 
  clist_hbox = gtk_hbox_new(1,1);
  gtk_paned_add1(GTK_PANED(paned), clist_hbox);
  
  clist = gtk_clist_new(NUM_TITLES);
  gtk_clist_new_with_titles(NUM_TITLES, titles);
  gtk_box_pack_end(GTK_BOX(clist_hbox), clist, TRUE, TRUE, 3);
  gtk_clist_set_policy(GTK_CLIST(clist), GTK_POLICY_ALWAYS, GTK_POLICY_AUTOMATIC);

  commentbox = aorta_make_commentbox();
  gtk_paned_add2(GTK_PANED(paned), commentbox->container);

  /*statusbar = aorta_make_statusbar();
  gtk_box_pack_end(GTK_BOX(vbox), statusbar->container, FALSE, FALSE, 0);
  */

  gtk_widget_show_all(app);
}

AortaCommentbox*
aorta_make_commentbox(void) {
  AortaCommentbox *commentbox;
  GtkWidget *table;
  GtkWidget *text;
  GtkWidget *vbox;
  GtkWidget *hbox;
  GtkWidget *label;
  GtkWidget *hscrollbar;
  GtkWidget *vscrollbar;

  commentbox = g_new(AortaCommentbox,1);

  hbox = gtk_hbox_new(TRUE,TRUE);

  vbox = gtk_vbox_new(TRUE,TRUE);
  gtk_box_pack_start(GTK_BOX(hbox), vbox, TRUE, TRUE, 3);

  table = gtk_table_new (3, 2, FALSE);
  gtk_box_pack_start(GTK_BOX(vbox), table, TRUE, TRUE, 3);
  gtk_table_set_row_spacing (GTK_TABLE (table), 0, 2);
  gtk_table_set_col_spacing (GTK_TABLE (table), 0, 2);
  
  label = gtk_label_new("Comments");
  gtk_misc_set_alignment (GTK_MISC(label),0.0,0.0);
  gtk_table_attach (GTK_TABLE (table), label, 0, 1, 0, 1,
                    GTK_EXPAND | GTK_FILL, GTK_FILL, 0, 0);
 
  text = gtk_text_new (NULL, NULL);
  gtk_widget_set_usize(text,1,1);
  gtk_widget_set_usize(table,1,1);
  gtk_table_attach_defaults (GTK_TABLE (table), text, 0, 1, 1, 2);

  hscrollbar = gtk_hscrollbar_new (GTK_TEXT (text)->hadj);
  gtk_table_attach (GTK_TABLE (table), hscrollbar, 0, 1, 2, 3,
                        GTK_EXPAND | GTK_FILL, GTK_FILL, 0, 0);

  vscrollbar = gtk_vscrollbar_new (GTK_TEXT (text)->vadj);
  gtk_table_attach (GTK_TABLE (table), vscrollbar, 1, 2, 1, 2,
                        GTK_FILL, GTK_EXPAND | GTK_FILL, 0, 0);

  commentbox->table = table;
  commentbox->vscroll = vscrollbar;
  commentbox->hscroll = hscrollbar;
  commentbox->text = text;
  commentbox->label = label;
  commentbox->vbox = vbox;
  commentbox->hbox = hbox;
  commentbox->container = commentbox->hbox;
 
  return commentbox;
}

AortaStatusbar* 
aorta_make_statusbar(void) {
  AortaStatusbar *statusbar;

  statusbar = g_new(AortaStatusbar,1);

  statusbar->frame = gtk_frame_new(NULL);
  gtk_frame_set_shadow_type(GTK_FRAME(statusbar->frame),GTK_SHADOW_OUT);

  statusbar->vbox = gtk_vbox_new(0,0);
  gtk_container_add(GTK_CONTAINER(statusbar->frame), statusbar->vbox);

  statusbar->hbox = gtk_hbox_new(0,0);
  gtk_box_pack_start(GTK_BOX(statusbar->vbox), statusbar->hbox, 
                     FALSE, FALSE, 1);

  statusbar->lframe = gtk_frame_new(NULL);
  gtk_frame_set_shadow_type(GTK_FRAME(statusbar->lframe),GTK_SHADOW_IN);
  gtk_box_pack_start(GTK_BOX(statusbar->hbox), statusbar->lframe, 
                     TRUE, TRUE, 1);

  statusbar->tlframe = gtk_frame_new(NULL);
  gtk_frame_set_shadow_type(GTK_FRAME(statusbar->tlframe),GTK_SHADOW_IN);
  gtk_box_pack_end(GTK_BOX(statusbar->hbox), statusbar->tlframe, 
                     FALSE, FALSE, 1);

  statusbar->label = gtk_label_new("Aorta!  Amazingly ORganized TAsks!");
  gtk_misc_set_alignment (GTK_MISC(statusbar->label),0.0,0.0);
  gtk_container_add(GTK_CONTAINER(statusbar->lframe), statusbar->label);

  statusbar->tlabel = gtk_label_new(" 2:30am");
  gtk_container_add(GTK_CONTAINER(statusbar->tlframe), statusbar->tlabel);
 
  statusbar->container = statusbar->frame;
  return statusbar;
}

AortaToolbarMain* 
aorta_make_main_toolbar(void) {
  AortaToolbarMain *toolbar;
  GtkWidget *tb_widget;

  toolbar = g_new(AortaToolbarMain,1);
  toolbar->handlebox = gtk_handle_box_new();

  toolbar->widget = gtk_toolbar_new(GTK_ORIENTATION_HORIZONTAL, 
                                    GTK_TOOLBAR_BOTH);
  gtk_container_add(GTK_CONTAINER(toolbar->handlebox),toolbar->widget);
  toolbar->container = toolbar->widget;

  tb_widget = toolbar->widget;
  gtk_toolbar_append_item(GTK_TOOLBAR(tb_widget), 
                          "Task", 
                          "Create a new root-level task", NULL,
                          NULL,NULL,NULL);
  gtk_toolbar_append_item(GTK_TOOLBAR(tb_widget), 
                          "Sub-Task", 
                          "Create a task under the currently selected task", NULL,
                          NULL,NULL,NULL);
  gtk_toolbar_append_space(GTK_TOOLBAR(tb_widget));
  gtk_toolbar_append_item(GTK_TOOLBAR(tb_widget), 
                          "Name", 
                          "Change the task name of the currently selected task",
                          NULL,NULL,NULL,NULL);
  gtk_toolbar_append_item(GTK_TOOLBAR(tb_widget), 
                          "Manager", 
                          "Change the manager of the currently selected task", NULL,
                          NULL,NULL,NULL);
  gtk_toolbar_append_space(GTK_TOOLBAR(tb_widget));
  gtk_toolbar_append_item(GTK_TOOLBAR(tb_widget), 
                          "Priority", 
                          "Change the priority of the currently selected task", NULL,
                          NULL,NULL,NULL);
  gtk_toolbar_append_item(GTK_TOOLBAR(tb_widget), 
                          "Status", 
                          "Change the status of the currently selected task", NULL,
                          NULL,NULL,NULL);
  gtk_toolbar_append_space(GTK_TOOLBAR(tb_widget));
  gtk_toolbar_append_item(GTK_TOOLBAR(tb_widget), 
                          "Start Date", 
                          "Change the start date of the currently selected task", NULL,
                          NULL,NULL,NULL);
  gtk_toolbar_append_item(GTK_TOOLBAR(tb_widget), 
                          "Deadline", 
                          "Change the deadline of the currently selected task", NULL,
                          NULL,NULL,NULL);

  return toolbar;
}
