/* Aorta -- Amazingly ORganized TAsks
 * Copyright (C) 1997 Shawn T. Amundson
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "database.h"
#include "interface.h"

int 
main (int argc, char *argv[]) 
{
  AortaTaskDB *taskdb;
  AortaTaskRecord *record;
  gint nchildren;
  gint child;
  /*AortaTaskID children;*/
  
  gnome_init("aorta", NULL, argc, argv, 0, NULL);

  /*taskdb = aorta_filedb_open_taskdb("taskdb.data");

  aorta_taskdb_children (taskdb, 0, &nchildren, &children);
  printf("nchildren: %d\n",nchildren);
  for (child = 0; child < nchildren; child++) {
    printf("child %d: id: %d\n", child, children[child]);
    record = aorta_taskdb_get_record (taskdb, children[child]);
    printf("child %d: name: %s\n", child, record->name);
  }
  aorta_taskdb_close(taskdb);
  */

  aorta_main_window();

  gtk_main();

  return 0;
}

