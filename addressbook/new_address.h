/* Aorta -- Amazingly ORganized TAsks
 * Copyright (C) 1998 Shawn T. Amundson
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */ 

#ifndef __AORTA_ADDRESSBOOK_NEW_ADDRESS__
#define __AORTA_ADDRESSBOOK_NEW_ADDRESS__

typedef struct _AortaAddressWindow AortaAddressWindow;

struct _AortaAddressWindow {
    GtkWidget *window;

    GtkWidget *entry_fullname;
    GtkWidget *entry_company;
    GtkWidget *entry_jobtitle;
    GtkWidget *entry_webpage;
    GtkWidget *entry_email;

    GtkWidget *combo_fileas;
    GtkWidget *combo_phone[6];
    GtkWidget *entry_phone[6];
};

void aorta_addressbook_gui_new_address (void); 

#endif /* __AORTA_ADDRESSBOOK_NEW_ADDRESS__ */
