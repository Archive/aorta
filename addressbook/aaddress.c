/* Aorta -- Amazingly ORganized TAsks
 * Copyright (C) 1997 Shawn T. Amundson
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */             

#include <gtk/gtk.h>                    
#include <gnome.h>
#include "new_address.h"
#include "menus.h"

void aorta_addressbook_open_new_window(void);

gint delete_event(GtkWidget *widget, GdkEvent *event, gpointer data);
void destroy (GtkWidget *widget, gpointer data);

int
main (int argc, char *argv[])
{   

  gnome_init("Aorta", NULL, argc, argv, 0, NULL);

  /*aorta_addressbook_open_new_window();*/ 
  aorta_addressbook_gui_new_address();

  gtk_main();

  return 0;
}   

void
aorta_addressbook_open_new_window() {
  GtkWidget *app;
  GtkWidget *menubar;
  GtkWidget *vbox;
  GtkWidget *clist;
  GtkWidget *statusbar;
  GtkMenuFactory *factory;
  GtkMenuFactory **subfactories;
  char *titles[2];
  guint context;

  titles[0] = "Name";
  titles[1] = "";

  app = gnome_app_new ("Aorta Address Book","Aorta Address Book");
  gtk_widget_set_usize(app, 550, 300);

  /* Menus */ 
  get_menubar(&menubar, NULL, 0);
  gnome_app_set_menus((GnomeApp*) app, GTK_MENU_BAR(menubar));

  /* Contents */
  vbox = gtk_vbox_new(0,4); 
  gnome_app_set_contents((GnomeApp*) app, vbox);

  clist = gtk_clist_new_with_titles(2, titles);
  gtk_box_pack_start(GTK_BOX(vbox), clist, 1,1,0);

  statusbar = gtk_statusbar_new();
  context = gtk_statusbar_get_context_id(GTK_STATUSBAR(statusbar), "Main Statusbar");
  gtk_statusbar_push(GTK_STATUSBAR(statusbar), context, "Aorta!");
  gtk_box_pack_start(GTK_BOX(vbox), statusbar, 0,1,0);

  gtk_widget_show_all(app);
}

gint
delete_event(GtkWidget *widget, GdkEvent *event, gpointer data)
{
    gint i;
    i = (gint) gtk_object_get_data(GTK_OBJECT(widget), "foo");
    printf("Delete Event... %d\n", i);
    return TRUE;
}

void destroy (GtkWidget *widget, gpointer data)
{
    gtk_main_quit ();
}


