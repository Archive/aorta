/* Aorta -- Amazingly ORganized TAsks
 * Copyright (C) 1998 Shawn T. Amundson
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */    

#include "Aorta.h"

class AddressBook_impl : virtual public Aorta::AddressBook_skel {
    CORBA::Long _calls;
    char *_book_title;
public:
    AddressBook_impl ();
    AddressBook_impl (CORBA::Object_ptr obj); 

    CORBA::Boolean _save_object ();
    void check_exit ();

    void book_title(const char *title);
    char* book_title();

    char* find (const char *regex);
};

class AddressBookLoader : public CORBA::BOAObjectRestorer {
public:
    CORBA::Boolean restore (CORBA::Object_ptr obj);
};

