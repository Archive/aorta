/* Aorta -- Amazingly ORganized TAsks
 * Copyright (C) 1998 Shawn T. Amundson
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */ 

#include "Aorta.h"

int main( int argc, char *argv[] )
{
    // ORB initialization
    CORBA::ORB_var orb = CORBA::ORB_init( argc, argv, "mico-local-orb" );
    CORBA::BOA_var boa = orb->BOA_init (argc, argv, "mico-local-boa");

    // client side
    assert (argc == 2);
    CORBA::Object_var obj = orb->bind ("IDL:Aorta/AddressBook:1.0", argv[1]);
    assert (!CORBA::is_nil (obj));
    Aorta::AddressBook_var client = Aorta::AddressBook::_narrow( obj );

    cout << "Search revealed: " << client->find("foo") << endl;

    client->book_title("Book 1");
    cout << "Book is: " << client->book_title() << endl;

    return 0;
}
