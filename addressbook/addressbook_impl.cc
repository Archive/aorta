/* Aorta -- Amazingly ORganized TAsks
 * Copyright (C) 1998 Shawn T. Amundson
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */    

#include <fstream.h>
#include "addressbook_impl.h"

AddressBook_impl::AddressBook_impl ()
    : Aorta::AddressBook_skel ()
{
    _calls = 0;
    _book_title = NULL;
}

AddressBook_impl::AddressBook_impl (CORBA::Object_ptr obj)
    : Aorta::AddressBook_skel (obj)
{
    cout << "restoring id " << obj->_ident() << endl;
    _calls = 0;
    ifstream inp (obj->_ident());
    assert (inp);
    inp >> _book_title;
}

CORBA::Boolean 
AddressBook_impl::_save_object ()
{
    cout << "saving id " << _ident() << endl;
    ofstream out (_ident());
    assert (out);
    out << _book_title;
    return TRUE;
}

void 
AddressBook_impl::check_exit ()
{
    if (++_calls > 5) {
        CORBA::BOA_var boa = _boa();
        /*
         * beware: while calling deactivate_impl() we can get
         * recursive invocations to the AddressBook methods ...
         */
        boa->deactivate_obj(this);
    }
}

char* 
AddressBook_impl::find (const char *regex)
{
    char *find;
    check_exit ();

    find = strdup("hello");
    return find;
}

char*
AddressBook_impl::book_title()
{
    return strdup(_book_title);
}

void
AddressBook_impl::book_title(const char *title)
{
    _book_title = strdup(title);
}

CORBA::Boolean
AddressBookLoader::restore (CORBA::Object_ptr obj)
{
    if (!strcmp (obj->_repoid(), "IDL:Aorta/AddressBook:1.0")) {
        new AddressBook_impl (obj);
        return TRUE;
    }
    cout << "cannot restore " << obj->_repoid() << " objects" << endl;
    return FALSE;
}

