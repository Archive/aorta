/* Aorta -- Amazingly ORganized TAsks
 * Copyright (C) 1998 Shawn T. Amundson
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */ 

#include <gtk/gtk.h>
#include <gnome.h>
#include "menus.h"
#include "new_address.h"
#include "../libaorta/db_address.h"

void button_ok_callback(GtkWidget *widget, gpointer data);
void button_cancel_callback(GtkWidget *widget, gpointer data);

void
aorta_addressbook_gui_new_address () {
  AortaAddressWindow *w;

  GtkWidget *menubar;
  GtkWidget *contents_box;
  GtkWidget *notebook;
  GtkWidget *general_label;
  GtkWidget *general_vbox;
  GtkWidget *general_sect1_table;
  GtkWidget *general_name_button;
  GtkWidget *general_company_label;
  GtkWidget *general_job_label;
  GtkWidget *general_job_entry;
  GtkWidget *general_fileas_label;
  GtkWidget *general_fileas_combo;
  GtkWidget *general_webpage_optionmenu; 
  GtkWidget *general_webpage_menu; 
  GtkWidget *general_webpage_menu_label[5];
  GtkWidget *general_webpage_entry;
  GtkWidget *general_email_optionmenu; 
  GtkWidget *general_email_menu; 
  GtkWidget *general_email_menu_label[5];
  GtkWidget *general_email_entry;
  GtkWidget *general_info_frame; 
  GtkWidget *general_info_text;
  GtkWidget *general_sep1;
  GList *fileas_list = NULL;
  GtkWidget *general_sect2_hbox;
  GtkWidget *general_phone_frame;
  GtkWidget *general_phone_table;
  GList *general_phone_list = NULL;
  GList *tlist = NULL;
  GtkWidget *general_phone_combo[6];
  GtkWidget *gen_menu;
  GtkWidget *gen_menu_label;
  GtkWidget *general_phone_entry[6];
  int i;
  GtkWidget *hbbox;
  GtkWidget *button_ok;
  GtkWidget *button_cancel;
  GtkWidget *vbox;
  GtkWidget *table;
 
  w = g_new(AortaAddressWindow,1);
  w->window = gnome_app_new("AortaAddressBook", "Aorta Address Book: New Address");

  /* Menus */
  get_menubar(&menubar, NULL, 1);
  gnome_app_set_menus((GnomeApp*) (w->window), GTK_MENU_BAR(menubar));

  /* Button Bar */
  
  contents_box = gtk_vbox_new(0,0);
  gtk_container_border_width(GTK_CONTAINER(contents_box), 4);
  gnome_app_set_contents((GnomeApp*) (w->window), contents_box);

  /* Notebook */
  notebook = gtk_notebook_new();
  gtk_box_pack_start(GTK_BOX(contents_box), notebook, 0, 0, 2);

  /* General Page */
  general_label = gtk_label_new("General");
  general_vbox = gtk_vbox_new(0,0);
  gtk_container_border_width(GTK_CONTAINER(general_vbox), 4);
  gtk_notebook_append_page(GTK_NOTEBOOK(notebook), general_vbox, general_label);

  general_sect1_table = gtk_table_new(7, 4, FALSE);
  gtk_table_set_col_spacing(GTK_TABLE(general_sect1_table), 0, 4);
  gtk_table_set_col_spacing(GTK_TABLE(general_sect1_table), 1, 8);
  gtk_table_set_col_spacing(GTK_TABLE(general_sect1_table), 2, 4); 
  gtk_table_set_row_spacings(GTK_TABLE(general_sect1_table), 4);
  gtk_box_pack_start(GTK_BOX(general_vbox), general_sect1_table, 0, 0, 2);

  general_name_button = gtk_button_new_with_label("Full Name..."); 
  w->entry_fullname = gtk_entry_new(); 
  gtk_table_attach(GTK_TABLE(general_sect1_table), general_name_button,
                   0, 1, 0, 1, GTK_FILL, 0, 0, 0);
  gtk_table_attach(GTK_TABLE(general_sect1_table), w->entry_fullname,
                   1, 2, 0, 1, GTK_FILL | GTK_EXPAND, 0, 0, 0);

  general_company_label = gtk_label_new("Company:");
  w->entry_company = gtk_entry_new();
  gtk_misc_set_alignment(GTK_MISC(general_company_label), 0.0, 0.0);
  gtk_table_attach(GTK_TABLE(general_sect1_table), general_company_label,
                   0, 1, 1, 2, GTK_FILL, 0, 0, 0);
  gtk_table_attach(GTK_TABLE(general_sect1_table), w->entry_company,
                   1, 2, 1, 2, GTK_FILL | GTK_EXPAND, 0, 0, 0);

  general_job_label = gtk_label_new("Job Title:");
  general_job_entry = gtk_entry_new();
  gtk_misc_set_alignment(GTK_MISC(general_job_label), 0.0, 0.0);
  gtk_table_attach(GTK_TABLE(general_sect1_table), general_job_label,
                   0, 1, 2, 3, GTK_FILL, 0, 0, 0);
  gtk_table_attach(GTK_TABLE(general_sect1_table), general_job_entry,
                   1, 2, 2, 3, GTK_FILL | GTK_EXPAND, 0, 0, 0);

  general_webpage_optionmenu = gtk_option_menu_new();
  general_webpage_menu = gtk_menu_new();
  general_webpage_menu_label[0] = gtk_menu_item_new_with_label("Web Page");
  gtk_menu_append(GTK_MENU(general_webpage_menu), general_webpage_menu_label[0]);
  general_webpage_menu_label[1] = gtk_menu_item_new_with_label("Web Page 2");
  gtk_menu_append(GTK_MENU(general_webpage_menu), general_webpage_menu_label[1]);
  general_webpage_menu_label[2] = gtk_menu_item_new_with_label("Web Page 3");
  gtk_menu_append(GTK_MENU(general_webpage_menu), general_webpage_menu_label[2]);
  general_webpage_menu_label[3] = gtk_menu_item_new_with_label("Web Page 4");
  gtk_menu_append(GTK_MENU(general_webpage_menu), general_webpage_menu_label[3]);
  general_webpage_menu_label[4] = gtk_menu_item_new_with_label("Web Page 5");
  gtk_menu_append(GTK_MENU(general_webpage_menu), general_webpage_menu_label[4]);
  gtk_widget_show(general_webpage_menu_label[0]);
  gtk_widget_show(general_webpage_menu_label[1]);
  gtk_widget_show(general_webpage_menu_label[2]);
  gtk_widget_show(general_webpage_menu_label[3]);
  gtk_widget_show(general_webpage_menu_label[4]);
  gtk_option_menu_set_menu(GTK_OPTION_MENU(general_webpage_optionmenu), 
                           general_webpage_menu);
  gtk_option_menu_set_history(GTK_OPTION_MENU(general_webpage_optionmenu), 0);
  general_webpage_entry = gtk_entry_new();
  gtk_table_attach(GTK_TABLE(general_sect1_table), general_webpage_optionmenu,
                   0, 1, 3, 4, GTK_FILL, 0, 0, 0);
  gtk_table_attach(GTK_TABLE(general_sect1_table), general_webpage_entry,
                   1, 2, 3, 4, GTK_FILL | GTK_EXPAND, 0, 0, 0);

  general_email_optionmenu = gtk_option_menu_new();
  general_email_menu = gtk_menu_new();
  general_email_menu_label[0] = gtk_menu_item_new_with_label("E-Mail");
  gtk_menu_append(GTK_MENU(general_email_menu), general_email_menu_label[0]);
  general_email_menu_label[1] = gtk_menu_item_new_with_label("E-Mail 2");
  gtk_menu_append(GTK_MENU(general_email_menu), general_email_menu_label[1]);
  general_email_menu_label[2] = gtk_menu_item_new_with_label("E-Mail 3");
  gtk_menu_append(GTK_MENU(general_email_menu), general_email_menu_label[2]);
  general_email_menu_label[3] = gtk_menu_item_new_with_label("E-Mail 4");
  gtk_menu_append(GTK_MENU(general_email_menu), general_email_menu_label[3]);
  general_email_menu_label[4] = gtk_menu_item_new_with_label("E-Mail 5");
  gtk_menu_append(GTK_MENU(general_email_menu), general_email_menu_label[4]);
  gtk_widget_show(general_email_menu_label[0]);  
  gtk_widget_show(general_email_menu_label[1]);
  gtk_widget_show(general_email_menu_label[2]);
  gtk_widget_show(general_email_menu_label[3]);
  gtk_widget_show(general_email_menu_label[4]);
  gtk_option_menu_set_menu(GTK_OPTION_MENU(general_email_optionmenu), 
                           general_email_menu);
  gtk_option_menu_set_history(GTK_OPTION_MENU(general_email_optionmenu), 0);
  general_email_entry = gtk_entry_new();
  gtk_table_attach(GTK_TABLE(general_sect1_table), general_email_optionmenu,
                   0, 1, 4, 5, GTK_FILL, 0, 0, 0);
  gtk_table_attach(GTK_TABLE(general_sect1_table), general_email_entry,
                   1, 2, 4, 5, GTK_FILL | GTK_EXPAND, 0, 0, 0);

  general_info_frame = gtk_frame_new("Info");
  gtk_table_attach(GTK_TABLE(general_sect1_table), general_info_frame,
                   0, 2, 5, 6, GTK_FILL | GTK_EXPAND, 0, 0, 0);
  general_info_text = gtk_text_new(NULL,NULL); 
  gtk_container_add(GTK_CONTAINER(general_info_frame), general_info_text);
  

  general_fileas_label = gtk_label_new("File As:");
  fileas_list = g_list_append(fileas_list, "Guster, Snorfle");
  general_fileas_combo = gtk_combo_new();
  gtk_combo_set_popdown_strings(GTK_COMBO(general_fileas_combo), fileas_list);
  gtk_misc_set_alignment(GTK_MISC(general_fileas_label), 0.0, 0.0);
  gtk_table_attach(GTK_TABLE(general_sect1_table), general_fileas_label,
                   2, 3, 0, 1, GTK_FILL, 0, 0, 0);
  gtk_table_attach(GTK_TABLE(general_sect1_table), general_fileas_combo,
                   3, 4, 0, 1, GTK_FILL | GTK_EXPAND, 0, 0, 0);
 
  general_sep1 = gtk_hseparator_new();
  gtk_box_pack_start(GTK_BOX(general_vbox), general_sep1, 0, 0, 2);

  general_sect2_hbox = gtk_hbox_new(0,0);
  gtk_box_pack_start(GTK_BOX(general_vbox), general_sect2_hbox, 0, 0, 0);

  vbox = gtk_vbox_new(0,0);
  gtk_table_attach(GTK_TABLE(general_sect1_table), vbox,
                   2, 4, 1, 6, GTK_FILL, 0, 0, 0);
  general_phone_frame = gtk_frame_new("Phone");
  gtk_box_pack_start(GTK_BOX(vbox), general_phone_frame, 0, 0, 0);
  

  general_phone_table = gtk_table_new(6, 2, FALSE);
  gtk_container_border_width(GTK_CONTAINER(general_phone_table), 4);
  gtk_container_add(GTK_CONTAINER(general_phone_frame), general_phone_table);
  gtk_table_set_row_spacings(GTK_TABLE(general_phone_table), 4);
  gtk_table_set_col_spacings(GTK_TABLE(general_phone_table), 4);
 
  general_phone_list = g_list_append(general_phone_list, "Home");
  general_phone_list = g_list_append(general_phone_list, "Business");
  general_phone_list = g_list_append(general_phone_list, "Fax");
  general_phone_list = g_list_append(general_phone_list, "Cell");
  for (i = 0; i < 6; i++) {
    general_phone_combo[i] = gtk_combo_new();
    tlist = g_list_first(general_phone_list);
    gtk_combo_set_popdown_strings(GTK_COMBO(general_phone_combo[i]), tlist);
    gtk_widget_set_usize(GTK_COMBO(general_phone_combo[i])->entry, 80, -1);
    general_phone_entry[i] = gtk_entry_new();
    gtk_table_attach(GTK_TABLE(general_phone_table), general_phone_combo[i],
                     0, 1, i, i+1, GTK_FILL, 1, 1, 1);
    gtk_table_attach(GTK_TABLE(general_phone_table), general_phone_entry[i],
                     1, 2, i, i+1, GTK_FILL | GTK_EXPAND, 0, 0, 0);
  }
  
  /*
  table = gtk_table_new(4,2,FALSE);
  gtk_box_pack_start(GTK_BOX(vbox), table, 0, 0, 0);
  */
  /*general_vbox_new = gtk_vbox_new();*/
  
  /*
  general_sect3_hbox = gtk_hbox_new(0,0);
  general_sect4_hbox = gtk_hbox_new(0,0);
  general_sect5_hbox = gtk_hbox_new(0,0);
  */

  /* Details Page */

  /* All Fields */


  /* OK & Cancel Buttons */ 

  /*
  hbbox = gtk_hbutton_box_new();
  gtk_container_border_width(GTK_CONTAINER(hbbox), 5);
  gtk_button_box_set_layout(GTK_BUTTON_BOX(hbbox), GTK_BUTTONBOX_END);
  gtk_box_pack_start(GTK_BOX(contents_box), hbbox, 0, 0, 0);
 
  button_ok = gtk_button_new_with_label("OK");
  gtk_container_add(GTK_CONTAINER(hbbox),button_ok);
  gtk_signal_connect (GTK_OBJECT (button_ok), "clicked",
                      GTK_SIGNAL_FUNC(button_ok_callback),
                      w);

  button_cancel = gtk_button_new_with_label("Cancel");
  gtk_container_add(GTK_CONTAINER(hbbox),button_cancel);
  gtk_signal_connect (GTK_OBJECT (button_cancel), "clicked",
                      GTK_SIGNAL_FUNC(button_cancel_callback),
                      w);
  */
  gtk_widget_show_all(w->window);
}

void
button_ok_callback(GtkWidget *widget, gpointer data) {
  AortaAddressWindow *w;
  AortaAddressRecord *record;

  w = (AortaAddressWindow*) data;
  record = g_new(AortaAddressRecord,1);

  record->name_last = gtk_entry_get_text(GTK_ENTRY(w->entry_fullname));
  record->company = gtk_entry_get_text(GTK_ENTRY(w->entry_company));

  printf("Full Name:  %s\n",record->name_last);
  printf("Company:    %s\n",record->company);
  
}

void
button_cancel_callback(GtkWidget *widget, gpointer data) {
  AortaAddressWindow *w;
  w = (AortaAddressWindow*) data;

}

