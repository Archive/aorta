/* Aorta -- Amazingly ORganized TAsks
 * Copyright (C) 1997 Shawn T. Amundson
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "db_address.h"

AortaAddressRecord* 
aorta_addressdb_get_record (AortaAddressDB *db, AortaAddressID id)
{
  return db->get_record(db, id);
}

void
aorta_addressdb_set_record (AortaAddressDB *db, 
	            	    AortaAddressID id,
                            AortaAddressRecord *record)
{
  return db->set_record(db, id, record);
}

void 
aorta_address_close (AortaAddressDB *db) 
{
  db->close(db);
}



#endif /* __AORTA_TASK_DB_H__ */
