/* Aorta -- Amazingly ORganized TAsks
 * Copyright (C) 1997 Shawn T. Amundson
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "db_task.h"

void
aorta_taskdb_close (AortaTaskDB *taskdb)
{
  taskdb->close(taskdb);
}

void
aorta_taskdb_children (AortaTaskDB *taskdb, 
                       AortaTaskID parent,
                       gint *nchildren, 
                       AortaTaskID **children) 
{
  gint child;
  taskdb->children(taskdb, parent, nchildren, children);
}

AortaTaskRecord*
aorta_taskdb_get_record (AortaTaskDB *taskdb, AortaTaskID id) 
{
  return taskdb->get_record(taskdb, id);
}

