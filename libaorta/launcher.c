/* Aorta -- Amazingly ORganized TAsks
 * Copyright (C) 1997 Shawn T. Amundson
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
 
#include "launcher.h"

void 
aorta_launch_task_manager (GtkWidget *widget, gpointer data)
{
    printf("Task Manager\n");
}

void 
aorta_launch_address_book (GtkWidget *widget, gpointer data)
{
    printf("Address Book\n");
}

void 
aorta_launch_day_planner(GtkWidget *widget, gpointer data)
{
    printf("Day Planner\n");
}

