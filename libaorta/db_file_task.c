/* Aorta -- Amazingly ORganized TAsks
 * Copyright (C) 1997 Shawn T. Amundson
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "db_file_task.h"
#include <glib.h>
#include <stdio.h>

AortaTaskRecord* aorta_filedb_taskdb_get_record(AortaTaskDB *db, 
                                                AortaTaskID id);
void aorta_filedb_taskdb_children (AortaTaskDB *taskdb,
                                   AortaTaskID parent,
                                   gint *nchildren,
                                   AortaTaskID **children);
void aorta_filedb_taskdb_close (AortaTaskDB *taskdb);
guint aorta_filedb_taskdb_hash (gpointer key);
void aorta_filedb_taskdb_load (AortaTaskDB *taskdb, char *filename);
char* get_next_token (FILE *fp);
void aorta_filedb_init (void);


typedef struct _AortaFileDBData AortaFileDBData;
struct _AortaFileDBData {
  char *filename;
 
  GHashTable *hash;
};

#define AORTA_NFIELDS 4
enum {
  AORTA_TOKEN_NAME,
  AORTA_TOKEN_PARENT,
  AORTA_TOKEN_STATUS,
  AORTA_TOKEN_PRIORITY
};

static char** token_names = NULL;

void
aorta_filedb_init (void)
{
  token_names = (gchar**) g_malloc(AORTA_NFIELDS * sizeof(gchar*));
  token_names[AORTA_TOKEN_NAME] = "name";
  token_names[AORTA_TOKEN_PARENT] = "parent";
  token_names[AORTA_TOKEN_STATUS] = "status";
  token_names[AORTA_TOKEN_PRIORITY] = "priority";
}

AortaTaskDB*
aorta_filedb_open_taskdb (char *filename) 
{
  AortaTaskDB *taskdb;
  AortaTaskRecord *record;
  AortaFileDBData *dbdata;

  if (token_names == NULL)
    aorta_filedb_init();

  taskdb = g_new(AortaTaskDB,1);

  /* Assign all functions for this type of db format */
  taskdb->get_record = aorta_filedb_taskdb_get_record;
  taskdb->children = aorta_filedb_taskdb_children;
  taskdb->close = aorta_filedb_taskdb_close;

  /* Save data specific to this type of database format */
  dbdata = g_new(AortaFileDBData, 1);
  dbdata->filename = g_strdup(filename);
  dbdata->hash = g_hash_table_new(aorta_filedb_taskdb_hash, NULL);
  taskdb->db_data = (gpointer) dbdata;

  /* Our actual task list */
  taskdb->record_list = g_list_alloc();

  /* Load the stuff here */
  aorta_filedb_taskdb_load(taskdb, filename);

  return taskdb;
}

void
aorta_filedb_taskdb_load (AortaTaskDB *taskdb, char *filename)
{
  FILE *fp;
  char ch;
  char *token;
  char *errorstr;
  int in_task;
  int token_key;
  int i;
  GList *record_list;
  GList *list;
  AortaTaskRecord *record;
  AortaTaskRecord *parent;
  AortaFileDBData *dbdata;

  errorstr = NULL;
  in_task = 0;

  record_list = g_list_alloc();

  if ((fp = fopen(filename, "r")) == NULL) {
    errorstr = "couldn't open file";
  } else {
 
    while ((errorstr == NULL) && 
           (token = get_next_token(fp)) && (token != NULL)) {
      
      if ((!in_task) && !strcmp(token, "task")) {
        in_task = 1;
        g_free(token);
        token = get_next_token(fp); /* TASKID, weeee... */
        if (token == NULL) {
          errorstr = "NULL token when expected taskid";
          break;
        }
        record = g_new(AortaTaskRecord, 1);
        record->id = atoi(token);
        record->children = g_list_alloc();
        record->parent = 0;
        record->name = NULL;
        record->priority = 5;
        record->status = "";
        g_free(token);
        token = get_next_token(fp);
        if ((token == NULL) || (strcmp(token,"{"))) {
          errorstr = "invalid token when expected '{'";
          if (token != NULL)
            g_free(token);
          g_free(record);
          break;
        }
      } else if (in_task) {
        if (!strcmp(token,"}")) {
          in_task = 0;

          g_list_append(record_list, (gpointer) record);
        } else {
          for (i = 0; i < AORTA_NFIELDS; i++) {
            token_key = -1;
            if (!strcmp(token_names[i], token)) {
              token_key = i;
              break;
            }
          }
          if (token_key == -1) {
            errorstr = "unknown token";
            g_free(token);
            break;
          } 
          g_free(token);
          token = get_next_token(fp); /* = */
          if (strcmp(token,"=")) {
            errorstr = "expected =, got something else";
            g_free(token);
            break;
          }
          g_free(token);
          token = get_next_token(fp); /* VALUE */
          switch (token_key)
          {
            case AORTA_TOKEN_NAME: 
              record->name = g_strdup(token);
              break;
            case AORTA_TOKEN_PARENT: 
              if (!strcmp(token,"0"))
                record->parent = 0;
              else
                record->parent = atoi(token);
              break;
            case AORTA_TOKEN_STATUS: 
              record->status = g_strdup(token);
              break;
            case AORTA_TOKEN_PRIORITY: 
              record->priority = atoi(token);
              break;
            default:
                ; /* an error really */
          }
        }
      } else {
        errorstr = "invalid token when expected 'task'";
        g_free(token);
        break;
      }
      g_free(token);
    }

    fclose(fp);
  }

  if (errorstr != NULL) {
    fprintf(stderr, "%s\n", errorstr);
    exit(1);
  }

  dbdata = (AortaFileDBData*) taskdb->db_data;
  list = record_list;
  while (list = g_list_next(list)) {
    record = (AortaTaskRecord*) list->data;
    if (record->parent == 0) {
      g_list_append(taskdb->record_list, (gpointer) record->id);
    } else {
      parent = aorta_filedb_taskdb_get_record(taskdb, record->parent); 
      g_list_append(parent->children, (gpointer) record->id);
    }
    g_hash_table_insert(dbdata->hash, (gpointer) record->id, (gpointer) record);  
  }
}

char*
get_next_token (FILE *fp) 
{
  char *token;
  char *tmptoken;
  char ch;
  int nchar;
  int endtok;
  int in_quote;
  int i;
 
  nchar = 256;
  token = (gchar*) g_malloc(nchar * sizeof(gchar));
  i = 0;
  endtok = 0;
  in_quote = 0;
 
  while ((!endtok) && (ch = getc(fp)) && (ch != EOF)) {
    switch (ch)
      {
      case '\\':
        ch = getc(fp);
        switch (ch)  
          {
            case '\n':
              break;
            default:
              if (i == (nchar - 1)) {
                token[i] = '\0';
                nchar += 256;
                tmptoken = (gchar*) g_malloc(nchar * sizeof(gchar));
                strcpy(tmptoken, token);
                g_free(token);
                token = tmptoken;
              }
              token[i++] = ch;
          }
        break;
      case '"':
        if (in_quote) {
          in_quote = 0;
          endtok = 1;
        } else {
          in_quote = 1;
        } 
        break;
      case ' ':
      case '\t':
      case '\n':
        if (!in_quote) {
          if (i)
            endtok = 1;
          break;
        }
      case '#':
        if (!in_quote) {
          while ((ch = getc(fp)) && (ch != EOF) && (ch != '\n')) {;} 
          break; 
        }
      default:
        if (i == (nchar - 1)) {
          token[i] = '\0';
          nchar += 256;
          tmptoken = (gchar*) g_malloc(nchar * sizeof(gchar));
          strcpy(tmptoken, token);
          g_free(token);
          token = tmptoken;
        }
        token[i++] = ch; 
      }
  }
  token[i] = '\0';

  /* copy token to malloc string of correct size */
  tmptoken = (gchar*) g_malloc((i+1) * sizeof(gchar));
  strcpy(tmptoken,token); 
  g_free(token);
  token = tmptoken;

  if (i == 0)  
    return NULL;
  else
    return token;
}

guint
aorta_filedb_taskdb_hash (gpointer key)
{
  /* I HAVE NO CLUE IF THIS IS RIGHT - Shawn */
  return (guint) (((gint)key) %101);
  /*
  return (guint) (key);
 <ctj> snorfle: try return (Guint) (key %101) and you will be doing much beter.
<Tylenol> or key ^ (key << 4)
<Tylenol> or (key * 5) ^ (key * 7)
  */
}

void
aorta_filedb_taskdb_children (AortaTaskDB *taskdb, 
                              AortaTaskID parentid,
                              gint *nchildren,
                              AortaTaskID **children) 
{
  AortaTaskRecord *parent;
  AortaTaskID *taskid;
  GList *child_list;

  gint nchild;
  gint child;

  *nchildren = 0; 
  *children = NULL;

  if (parentid == 0) {
    child_list = taskdb->record_list;
  } else {
    parent = aorta_taskdb_get_record(taskdb, parentid); 
    g_return_if_fail(parent != NULL);
    /*g_return_if_fail(parent->children != NULL);*/
    child_list = parent->children;
  }
  /* go through children, put them in newly created array */

  nchild = g_list_length(child_list) - 1;
  *children = (AortaTaskID*) g_malloc(sizeof(AortaTaskID) * nchild);
  for (child = 0; child < nchild; child++) {
    taskid = (AortaTaskID*) g_list_nth(child_list, child+1);
    (*children)[child] = *taskid;
  }

  *nchildren = nchild;
}

AortaTaskRecord* 
aorta_filedb_taskdb_get_record (AortaTaskDB *db, AortaTaskID id) 
{
  AortaTaskRecord *record;
  record = (AortaTaskRecord*) g_hash_table_lookup(((AortaFileDBData*)(db->db_data))->hash, (gpointer) id);
  return record;
};

void
aorta_filedb_taskdb_close (AortaTaskDB *taskdb) {
  g_list_free (taskdb->record_list);
  g_free (((AortaFileDBData*)taskdb->db_data)->filename);
  g_free (taskdb->db_data);
  g_free (taskdb);
}

