/* Aorta -- Amazingly ORganized TAsks
 * Copyright (C) 1997 Shawn T. Amundson
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __AORTA_TASK_DB_H__
#define __AORTA_TASK_DB_H__

#include <glib.h>

typedef struct _AortaTaskRecord AortaTaskRecord;
typedef struct _AortaTaskDB AortaTaskDB;
typedef struct _AortaDate AortaDate;
typedef guint AortaTaskID;

struct _AortaDate { /* Um, think this out out more. */
  gint day;
  gint month;
  gint year;
};

struct _AortaTaskRecord {
  AortaTaskID id; 
  AortaTaskID parent;

  GList *children;

  char *name;
  char *status;
  guint priority; /* 1 Urgent - 10 Low (or similar) */

  AortaDate *startdate;
  AortaDate *deadline;

  char *comments; /* Perhaps make this into a tree of comments? 
                     Must figure out how the interface for comments
                     should look before deciding.
                   */
};

struct _AortaTaskDB {
  AortaTaskRecord* (* get_record) (AortaTaskDB*, AortaTaskID);
  void  (* children) (AortaTaskDB*, 
                      AortaTaskID, 
                      gint*, 
                      AortaTaskID**);
  void (* close) (AortaTaskDB*);

  GList *record_list;
  GList *current;

  gpointer *db_data;
};

AortaTaskRecord* aorta_taskdb_get_record(AortaTaskDB *db, AortaTaskID id);
void aorta_taskdb_close (AortaTaskDB *taskdb);
void aorta_taskdb_children (AortaTaskDB *taskdb, 
                            AortaTaskID parent,
                            gint *nchildren,
                            AortaTaskID **children);


#endif /* __AORTA_TASK_DB_H__ */
