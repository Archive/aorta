/* Aorta -- Amazingly ORganized TAsks
 * Copyright (C) 1997 Shawn T. Amundson
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __AORTA_ADDRESS_DB_H__
#define __AORTA_ADDRESS_DB_H__

#include <glib.h>

typedef struct _AortaAddressRecord AortaAddressRecord;
typedef struct _AortaAddressDB AortaAddressDB;
typedef guint AortaAddressID;

#define AORTA_ADDRESS_NFIELDS 49

enum {
  AORTA_AF_NAME,
  AORTA_AF_FIRSTNAME,
  AORTA_AF_MIDDLENAME,
  AORTA_AF_LASTNAME,
  AORTA_AF_MAIDENNAME,
  AORTA_AF_MRMRS,
  AORTA_AF_TITLE,
  AORTA_AF_ADDON,
  AORTA_AF_COMPANY,
  AORTA_AF_INSTITUTE,
  AORTA_AF_DEPARTMENT,
  AORTA_AF_POBOX,
  AORTA_AF_STREET,
  AORTA_AF_CITY,
  AORTA_AF_ZIP,
  AORTA_AF_COUNTY,
  AORTA_AF_STATE,
  AORTA_AF_PROVINCE,
  AORTA_AF_COUNTRY,
  AORTA_AF_BIRTHDAY,
  AORTA_AF_CONTACT0,
  AORTA_AF_CONTACT1,
  AORTA_AF_CONTACT2,
  AORTA_AF_CONTACT3,
  AORTA_AF_CONTACT4,
  AORTA_AF_CONTACT5,
  AORTA_AF_CONTACT6,
  AORTA_AF_CONTACT7,
  AORTA_AF_CONTACT8,
  AORTA_AF_CONTACT9,
  AORTA_AF_WWW,
  AORTA_AF_REMARK,
  AORTA_AF_OTHER0,
  AORTA_AF_OTHER1,
  AORTA_AF_OTHER2,
  AORTA_AF_OTHER3,
  AORTA_AF_OTHER4,
  AORTA_AF_OTHER5,
  AORTA_AF_OTHER6,
  AORTA_AF_OTHER7,
  AORTA_AF_OTHER8,
  AORTA_AF_OTHER9,
  AORTA_AF_CATEGORY,
  AORTA_AF_ALIAS,
  AORTA_AF_ID,
  AORTA_AF_DATE_ENTERED,
  AORTA_AF_DATE_LASTCONTACTED,
  AORTA_AF_DATE_LASTCHANGE
};

enum {
  AORTA_AF_CT_HOME,
  AORTA_AF_CT_WORK,
  AORTA_AF_CT_MOBILE,
  AORTA_AF_CT_PAGER,
  AORTA_AF_CT_FAX,
  AORTA_AF_CT_SECRETARY,
  AORTA_AF_CT_OFFICE,
  AORTA_AF_CT_EMAIL
};

struct _AortaAddressRecord {
  AortaAddressID id; 

  gchar *name_title;
  gchar *name_first;
  gchar *name_middle;
  gchar *name_last;
  gchar *name_suffix;

  gchar *company;

  gchar **fields;
};

struct _AortaAddressDB {
  AortaAddressRecord* (* get_record) (AortaAddressDB*, AortaAddressID);
  void (* set_record) (AortaAddressDB*, AortaAddressID, AortaAddressRecord*);
  void (* close) (AortaAddressDB*);

  GList *record_list;

  gint nfields;
  gint **ntitles;
  char ***field_titles;

  gpointer *db_data;
};

AortaAddressRecord* aorta_addressdb_get_record(AortaAddressDB *db, 
                                               AortaAddressID id);
void aorta_addressdb_set_record (AortaAddressDB *db,
                                 AortaAddressID id,
                                 AortaAddressRecord *record);
void aorta_addressdb_close (AortaAddressDB *db);


#endif /* __AORTA_TASK_DB_H__ */
