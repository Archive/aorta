
Aorta Task Manager Design
=========================

Introduction
------------

The main emphasis of the task manager is the ability to keep a 
personal task list.  It is very important to be able to prioritize
what needs to be done, and it is likely that a person using such
software will wish to see at a glance what they need to work on.

We must clarify where this task list information is coming from.
The tasks come from everything from short little things you could
put on a post-it-note to long-term group-oriented projects.

The ability to assign tasks to individual users is extremely 
important.  Other important aspects are the ability to track
time related to each task and organizing tasks by deadline and
priority.

Task Oriented
-------------

Currently there are few task planning programs for the UNIX 
environment.  The ones worth mentioning are: plan, ical, and
gtt.  Plan and Ical are both from a time-based rather than a
task-based point of view; see Aorta's day planner for more
information on that point of view.  

While plan has fairly good support for groups, if you wish 
to do a to-do list in it you are out of luck.  You can, however,
keep a to-do list in ical, but working with groups is very 
difficult.  Neither of these is useful for organizing or
estimating times of projects.

Gtt is an awesome utility, and is simple and to the point: it 
tracks your time.  It is task based, but contains too little 
information for long-term projects as that is not the goal of
the application.

A main focus of the task manager is the ability to very quickly
view your personal to-do list, including the ability to time
yourself while preforming these tasks.  This will allow easy
generation of reports about time usage.  These reports may
be very useful in billing people for time worked on a project
for example.

Definition of a task
--------------------

Tasks contain the following information: [THIS NEEDS WORK]

       Parent-task 
       Sub-tasks
       Task Dependancies (Task that must be completed first)
       Priority 
       Task Manager (Responsible person)
       Related Data List (URLs, Files, etc.) - in the comment block?
       Comment-block 
       Suggested Start Date 
       Time Estimate
       Age of task
       Time spent on task 
       Deadline (specify date or open)
       Status 
       People involved
       Groups involved (perhaps this should be categories for
                        reasons explained in the Atlas design
                        info)

The following are different modes or views in the program:

       Personal View 
              To-do List 
       Project/Task View 
              Tasks by user 
              Tasks by group  (task by category?)
              Reports 
              Gantt Chart 
       User Info View 
              Contact info 
              Current workload 
              Upcoming unavailability 

[this isn't complete]
