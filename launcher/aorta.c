/* Aorta -- Amazingly ORganized TAsks
 * Copyright (C) 1997 Shawn T. Amundson
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <gtk/gtk.h>
#include <gnome.h>
#include "aorta-day-planner.xpm"
#include "aorta-address-book.xpm"
#include "aorta-heart.xpm"
#include "libaorta/launcher.h"

gint delete_event(GtkWidget *widget, gpointer data);
void destroy (GtkWidget *widget, gpointer data);

#define N_BUTTONS 3
#define NUM_MENU 2

enum {
  DAY_PLANNER,
  ADDRESS_BOOK,
  TASK_MANAGER
};

static GtkMenuFactory *factory = NULL;
static GtkMenuFactory *subfactories[NUM_MENU];

static GtkMenuEntry menu_entries[] =
{
        {"<Main>/File/Day Planner...", NULL, aorta_launch_day_planner, NULL},
        {"<Main>/File/Address Book...", NULL, aorta_launch_address_book, NULL},
        {"<Main>/File/Task Manager...", NULL, aorta_launch_task_manager, NULL},
        {"<Main>/File/<separator>", NULL, NULL, NULL},
        {"<Main>/File/Exit", NULL, destroy, NULL},
        {"<Main>/Help/About Aorta...", NULL, NULL, NULL}
};
static int nmenu_entries = sizeof (menu_entries) / sizeof (menu_entries[0]);


int 
main (int argc, char *argv[]) 
{
  GtkWidget *app;
  GtkWidget *vbox;
  GtkWidget *menubar;
  GtkMenuPath *menupath;
  GtkWidget *toolbar;
  GtkWidget *statusbar;
  GtkWidget *heart_pixmap_w;
  GdkPixmap *heart_pixmap;
  GdkBitmap *heart_bitmap;
  GtkWidget *pixmap_w[N_BUTTONS];
  GdkPixmap *pixmap[N_BUTTONS];
  GdkBitmap *bitmap[N_BUTTONS];
  char *text[N_BUTTONS];
  char **xpm_data[N_BUTTONS];
  GtkSignalFunc *func[N_BUTTONS];
  guint context;
  int i;

  /*gtk_init(&argc, &argv);*/
  gnome_init("Aorta", NULL, argc, argv, 0, NULL);

  app = gnome_app_new("Aorta", "Aorta Suite");
  gtk_signal_connect (GTK_OBJECT (app), "delete_event",
                      GTK_SIGNAL_FUNC (delete_event), NULL);
  gtk_signal_connect (GTK_OBJECT (app), "destroy",
                      GTK_SIGNAL_FUNC (destroy), NULL);

  factory = gtk_menu_factory_new (GTK_MENU_FACTORY_MENU_BAR); 
  subfactories[0] = gtk_menu_factory_new (GTK_MENU_FACTORY_MENU_BAR);
  gtk_menu_factory_add_subfactory (factory, subfactories[0], "<Main>");
  gtk_menu_factory_add_entries (factory, menu_entries, nmenu_entries);
  menupath = gtk_menu_factory_find(factory, "<Main>/Help");
  if (menupath) 
    gtk_menu_item_right_justify(GTK_MENU_ITEM(menupath->widget));
  gnome_app_set_menus((GnomeApp*) app, GTK_MENU_BAR(subfactories[0]->widget));

  vbox = gtk_vbox_new(0,0);
  gnome_app_set_contents((GnomeApp*) app, vbox);

  toolbar = gtk_toolbar_new(GTK_ORIENTATION_HORIZONTAL,
                            GTK_TOOLBAR_ICONS);
  gtk_box_pack_start(GTK_BOX(vbox), toolbar, 0, 0, 0);
  gtk_container_border_width(GTK_CONTAINER(toolbar), 4);

  statusbar = gtk_statusbar_new();
  context = gtk_statusbar_get_context_id(GTK_STATUSBAR(statusbar), "Main Statusbar");
  gtk_box_pack_start(GTK_BOX(vbox), statusbar, 0, 0, 0);
  gtk_statusbar_push(GTK_STATUSBAR(statusbar), context, "Aorta!");

  heart_pixmap = gdk_pixmap_create_from_xpm_d(statusbar->window, &heart_bitmap, 
                      &gtk_widget_get_style (statusbar)->bg[GTK_STATE_NORMAL],
                      aorta_heart_xpm);
  heart_pixmap_w = gtk_pixmap_new(heart_pixmap,heart_bitmap);
  gtk_box_pack_end(GTK_BOX(statusbar), heart_pixmap_w, 0,0,0);

  xpm_data[DAY_PLANNER] = aorta_day_planner_xpm;
  xpm_data[TASK_MANAGER] = aorta_day_planner_xpm;
  xpm_data[ADDRESS_BOOK] = aorta_address_book_xpm;
  
  text[DAY_PLANNER] = "Day Planner";
  text[TASK_MANAGER] = "Task Manager";
  text[ADDRESS_BOOK] = "Address Book";

  func[DAY_PLANNER] = GTK_SIGNAL_FUNC(aorta_launch_day_planner); 
  func[TASK_MANAGER] = GTK_SIGNAL_FUNC(aorta_launch_task_manager); 
  func[ADDRESS_BOOK] = GTK_SIGNAL_FUNC(aorta_launch_address_book); 

  for (i = 0; i < N_BUTTONS; i++) {
    pixmap[i] = gdk_pixmap_create_from_xpm_d(toolbar->window, &bitmap[i], 
                      &gtk_widget_get_style (toolbar)->bg[GTK_STATE_NORMAL],
                      xpm_data[i]);  
    pixmap_w[i] = gtk_pixmap_new(pixmap[i],bitmap[i]);
    gtk_toolbar_append_item (GTK_TOOLBAR(toolbar), NULL, text[i], NULL,
                             GTK_PIXMAP(pixmap_w[i]), 
                             GTK_SIGNAL_FUNC(func[i]), NULL);
    if (i != (N_BUTTONS -1))
      gtk_toolbar_append_space(GTK_TOOLBAR(toolbar));
    
  }

  gtk_widget_show_all(app);

  gtk_main();

  return 0;
}

gint 
delete_event(GtkWidget *widget, gpointer data)
{
    gint i;
    i = (gint) gtk_object_get_data(GTK_OBJECT(widget), "foo");
    printf("Delete Event... %d\n", i);
    return TRUE;
}

void destroy (GtkWidget *widget, gpointer data)
{
    gtk_main_quit ();
}
